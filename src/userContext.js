
import React from "react";

const UserContext = React.createContext();
// a context object is a data type of an object that can be used to store information that can be shared to other components within the app
// ^ is a different approach to passing information between componentsand allows easier access by avoiding prop-drillings

export const UserProvider = UserContext.Provider;
// "provider" component allows other components to consume/use the context object and supply the necessary information needed to the context object

export default UserContext;