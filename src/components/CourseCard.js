
import { Row, Col, Card, Button } from "react-bootstrap";
// import { useState } from "react";
import { Link } from "react-router-dom";

export default function CourseCard({courseProp}){
// argument data is from courseProp on course page
/*
	"props" contains
	courseProp = {object with indicated index}
*/

// can instead directly insert CourseCard(props) as argument for direct referencing (w/o deconstruction), then:
/*
		<Card.Title>{props.courseProp.name}</>
		<Card.Text>{props.courseProp.description}</>
		<Card.Text>{props.courseProp.price}</>
*/

// props / properties act as function parameter

// -----------------------------

const { name, description, price, _id } = courseProp;

	// const [count, setCount] = useState(0);
	// always uses a getter (count) and a setter (setCount)
	// getter = stores the value
	// setter = tells how to store/set the value in the getter
	// 0 from the useState() is the initial getter value

	// const [seats, setSeats] = useState(30);

	// function enroll(){
	// 	if(seats === 0){
	// 		alert("No more seats available.")
	// 	}
	// 	else{
	// 		setCount(count + 1);
	// 		setSeats(seats - 1);
	// 		// console.log(`Enrollees: ${count}`);
	// 	}
	// }

// -----------------------------

	return(
		<Row className="mt-3 mb-3">
		<Col xs={12}>
			<Card className="cardHighlight p-2">
		      	<Card.Body>

		      		<Card.Title className="mb-1">{name}</Card.Title>

			        <Card.Subtitle className="mb-1 mt-2">Description:</Card.Subtitle>
			        <Card.Text className="mb-3">
			          {description}
			        </Card.Text>

			        <Card.Subtitle className="mb-1">Price:</Card.Subtitle>
			        <Card.Text className="mb-3">
			          {price}
			        </Card.Text>

			        <Button variant="primary" as={Link} to={`/courseView/${_id}`}>Details</Button>

		      	</Card.Body>
		    </Card>
	    </Col>
	    </Row>
	)
}