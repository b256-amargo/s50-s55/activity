
// REACT BOOTSTRAP COMPONENTS
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
// can also be shortened (deconstruct) to:
// import { Container, Navbar, Nav } from "react-bootstrap"

import { Link, NavLink } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../userContext";

export default function AppNavBar(){

	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);
	// state hooks for storing user info in the login page

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
      		<Container>
		        <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          	<Nav className="me-auto">
		          	
		            	<Nav.Link as={NavLink} to="/">Home</Nav.Link>
		            	<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

		            	{
		            		(user.id !== null) ?
		            			<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
		            		:
		            		<>
		            			<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
		            			<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		            		</>
		            	}
		            	
		          	</Nav>
		        </Navbar.Collapse>
      		</Container>
    	</Navbar>
    	// copied from react bootstrap components documentation (website)
	)
	// return statement ALWAYS required
}