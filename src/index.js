
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import "bootstrap/dist/css/bootstrap.min.css"; // bootstrap css

const root = ReactDOM.createRoot(document.getElementById('root'));
// connects to the index.html file

// ----------------------------------

root.render(
  <React.StrictMode>
  	<App />
  </React.StrictMode>
);
//App component is our mother component, this is the component we use as entry point and where we can render all other components or pages.

// ----------------------------------

// const name = "John Smith";
// const user = {
// 	firstName: "Jane",
// 	lastName: "Smith"
// }

// function formatName(user){
// 	return user.firstName + " " + user.lastName;
// }

// const element = <h1>Hello, {name} and {formatName(user)}</h1>
// // JSX allows us to create HTML elements and at the same time allows us to apply JavaScript code to these elements making it easy to write both HTML and JavaScript code in a single file as opposed to creating two separate files (One for HTML and another for JavaScript syntax)

// root.render(element);
// //render() - displays the react elements/components into the root.
// // content of the "element" was essentially transferred to the index.html body div with the root ID