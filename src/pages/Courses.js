
// import CourseData from "../data/CourseData";
import CourseCard from "../components/CourseCard";
import { useState, useEffect } from "react";

export default function Courses(){

	// console.log(CourseData);

	const [ courses, setCourses ] = useState([]);

	useEffect(() => {
		fetch("http://localhost:4000/courses/active")
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			// will continously log on console
			setCourses(data.map(course => {
				return(
					<CourseCard key={course.id} courseProp={course} />
				)
			}))
		})
	});

	return(
		<>
		<h1>Courses</h1>
		{courses}
		</>
	)
}