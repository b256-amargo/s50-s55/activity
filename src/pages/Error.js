
// import { Row, Col } from "react-bootstrap";
// import { Link, NavLink } from "react-router-dom";
import Banner from '../components/Banner';

export default function Error(){

	// return(
	// 	<Row>
	// 		<Col className="p-5 text-center">
	// 			<h1>Page Not Found</h1>
	// 			<p>Go back to the <Link as={NavLink} to="/">homepage</Link>.</p>
	// 		</Col>
	// 	</Row>
	// )

	const bannerData = {
		title: "404 - Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back Home"
	}

	return(
		<Banner data={bannerData} />
	)
}