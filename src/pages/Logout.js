
import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../userContext";

export default function Logout(){

	// localStorage.clear();
	// clears out all content of local storage (can be seen on browser's console)

	const { unsetUser, setUser } = useContext(UserContext);
	unsetUser();
	// only clears the localStorage, email is still saved on user itself

	useEffect(() => {
		setUser({
			id: null
		})
	})
	// sets the user email property to null

	return(
		<Navigate to="/login" />
	)
}