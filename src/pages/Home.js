
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const homeData = {
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll now!"
    }

    return (
        <>
            <Banner data={homeData}/>
            <Highlights />
        </>
    )

}