
import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function Login(){

	const { user, setUser } = useContext(UserContext);
	// allows consuming UserContext object and its properties for user validation

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	// state hooks for storing input field value

	const [isActive, setIsActive] = useState(false);
	// state for determining if submit button is enabled or not

	// --------------------------------

	useEffect (() => {
		if(email !== "" && password !== "") {
			setIsActive(true);
		}
		else{
			setIsActive(false);
		};
	});
	// gets triggered whenever there is a change in the website

	// --------------------------------

	function loginUser(e){
		e.preventDefault();

		fetch("http://localhost:4000/users/login", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
			// values are from input field
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			// will log the access token

			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Authentication Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			}
			else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
			// If no user information is found, the "access" property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
		});
		// fetching the corresponding backend API

		// localStorage.setItem("email", email);
		// setting the email of the authenticated user in the local storage

		// setUser({
		// 	email: localStorage.getItem("email")
		// });

		setEmail("");
		setPassword("");

		// alert("You are now logged in!");
	};

	// --------------------------------

	const retrieveUserDetails = (token) => {
		fetch("http://localhost:4000/users/details", {
			headers: {
				Authorization: `Bearer ${token}`
			}
			// The token will be sent as part of the request's header information
			// We put "Bearer" in front of the token to follow implementation standards for JWTs
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			// values are from access token's payload
			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
		})
	};

	// --------------------------------

	return(
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
		<Form onSubmit={e => loginUser(e)}>

			<h1 className="mt-3 mb-3">Login</h1>
			
	      	<Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
	        	<Form.Label>Email Address</Form.Label>
	        	<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
	        	<Form.Label>Password</Form.Label>
	        	<Form.Control type="password" placeholder="Enter password" value={password} onChange={e => setPassword(e.target.value)}/>
	      	</Form.Group>

	      	{
	      		isActive ?
	      			<Button variant="success" type="submit" id="submitBtn">
			        Login
			      	</Button>
			    :
	      			<Button variant="success" type="submit" id="submitBtn" disabled>
			        Login
			      	</Button>
	      	}

	    </Form>
	)
}