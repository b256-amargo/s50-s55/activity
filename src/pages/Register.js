
import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function Register(){
	const { user } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// state hooks for storing the values of the input fields

	// ----------------
	// ACTIVITY s55
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const navigate = useNavigate();

	const userRegisterForm = [firstName, lastName, mobileNo, email, password1, password2];
	let checkUserForm = userRegisterForm.every(function(detail) {
        return (detail);
    });
	// ----------------

	const[isActive, setIsActive] = useState(false);
	// state to determine if submit button is enabled/not

	useEffect (() => {
		// if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
		// 	setIsActive(true);
		// }
		// else{
		// 	setIsActive(false);
		// };

		// ----------------
		// ACTIVITY s55
		if((checkUserForm === true) && (mobileNo.length >= 11) && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	});
	// useEffect() activates whenever there is a change in the webpage

	function registerUser(e){
		e.preventDefault();

		// ----------------
		// ACTIVITY s55
		fetch("http://localhost:4000/users/checkEmail", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(!data){
				Swal.fire({
					title: "Email already exists!",
					icon: "error",
					text: "Please provide a different email"
				})
			}
			else{
				fetch("http://localhost:4000/users/register", {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You have successfully registered"
				})
				navigate("/login")
			}
		})
		// ----------------

		setFirstName("");
		setLastName("");
		setMobileNo("");
		setEmail("");
		setPassword1("");
		setPassword2("");
		// for clearing input fields
	}
	// function to be activated whenever the whole form input gets submitted (button)

	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	return(
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
		<Form onSubmit={e => registerUser(e)}>

			{/*ACTIVITY s55*/}
			<Form.Group className="mt-3 mb-3" controlId="formBasicFirstName">
	        	<Form.Label>First Name</Form.Label>
	        	<Form.Control type="string" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
	      	</Form.Group>

	      	{/*ACTIVITY s55*/}
	      	<Form.Group className="mt-3 mb-3" controlId="formBasicLastName">
	        	<Form.Label>Last Name</Form.Label>
	        	<Form.Control type="string" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)}/>
	      	</Form.Group>

	      	{/*ACTIVITY s55*/}
	      	<Form.Group className="mt-3 mb-3" controlId="formBasicMobileNo">
	        	<Form.Label>Mobile Number</Form.Label>
	        	<Form.Control type="string" placeholder="Enter mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="formBasicEmail">
	        	<Form.Label>Email address</Form.Label>
	        	<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        	{/*
	        	// onChange - checks if there are any changes inside of the input fields.
				// value={email} - the value stroed in the input field will come from the value inside the getter "email"
				// setEmail(e.target.value) - sets the value of the getter ëmail to the value stored in the value={email} inside the input field
				*/}
	        	<Form.Text className="text-muted">
	        	We'll never share your email with anyone else.
	        	</Form.Text>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="formBasicPassword">
	        	<Form.Label>Password</Form.Label>
	        	<Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="formBasicPassword2">
	        	<Form.Label>Verify Password</Form.Label>
	        	<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	      	</Form.Group>

	      	{
	      		isActive ?
	      			<Button variant="primary" type="submit" id="submitBtn">
			        Submit
			      	</Button>
			    :
	      			<Button variant="danger" type="submit" id="submitBtn" disabled>
			        Submit
			      	</Button>
	      	}
		    {/*Ternary Operator (for Boolean values only)*/}
		    {/*
				if (isActive === true){
					button variant primary
				}
				else{
					button variant danger
				}
		    */}

	    </Form>
	)
}