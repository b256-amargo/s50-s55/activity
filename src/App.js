
import './App.css';
import AppNavBar from "./components/AppNavBar";
import { UserProvider } from "./userContext";
import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
// The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser. Renamed as "Router" via 'as'
// The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint.

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import CourseView from "./pages/CourseView";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";

// ---------------------------------------

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  // user state hook (global)
  // value will be filled later from token payload upon logging in
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not

  const unsetUser = () => {
    localStorage.clear();
  };
  // for clearing localStorage when a user logs out

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  });

	return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
    		<Router>

      		<AppNavBar />

      		<Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              <Route path="/courseView/:courseId" element={<CourseView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<Error />} />
            </Routes>
      		</Container>
      	</Router>
      </UserProvider>
    </>
  	// <> are fragments. Used when there's more than one HTML element (components, pages, etc)
	);
}
// functions should always be paired with a return statement inside it
// browser router (router) should always include routes then individual route inside 

export default App;
